import { ObjectId } from 'mongodb';
import { prepare } from '../utils';

const createProfile = async (parent, args, { db }) => {
  const user = prepare(await (db.collection('users').findOne({ _id: ObjectId(args.id) })));
  if (user) {
    args.userId = ObjectId(args.id);
    delete args.id
    const res = await db.collection('profiles').insertOne({ ...args });
    return { id: res.insertedId }
  }
  throw new Error("Can't create profile!")
}

export default createProfile;