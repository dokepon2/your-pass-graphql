import * as validator from 'validator';
import * as bcrypt from 'bcryptjs';
import { signToken } from '../utils';

const signupUser = async (parent, { email, password }, { db }) => {
  email = email.toLowerCase();
  if (!validator.isEmail(email)) {
    throw new Error('Not a valid email')
  }

  const existsUser = await db.collection('users').findOne({ email })
    .then(r => r);

  if (existsUser) {
    throw new Error('Email already in use');
  }

  const hash = await bcrypt.hash(password, 8)

  const userId = await db.collection('users').insertOne({ email, password: hash, roles: ['USER'] }).then(r => r.insertedId);
  console.log(userId)

  return {
    id: userId,
    token: signToken({ userId, roles: ['USER'] })
  }
}

export default signupUser;