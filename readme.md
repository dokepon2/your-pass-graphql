## Yourpass GraphQL Server

### Running it:

- Use `docker-compose up` to setup mongodb.
- Use `yarn start` to start the server in Developer Mode.

### Building it and Serving it for Production:

- Use `yarn build` to compile it with babel
- Use `yarn serve` to run the server.

### Dependencies:

- Graphql-Yoga
- Mongodb
- Babel
- Nodemon


