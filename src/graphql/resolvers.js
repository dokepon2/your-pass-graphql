import user from './account/user';
import users from './account/users';
import authenticateUser from './account/authenticateUser';
import signupUser from './account/signupUser';
import changePassword from './account/changePassword';
import createProfile from './account/createProfile';
import createDocument from './account/createDocument';
import createAchievement from './account/createAchievement';

export default {
  Query: {
    user,
    users
  },
  Mutation: {
    authenticateUser,
    signupUser,
    changePassword,
    createProfile,
    createDocument,
    createAchievement
  }
};