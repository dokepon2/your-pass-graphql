import { ObjectId } from 'mongodb';
import user from './user';

const createDocument = async (parent, args, ctx) => {
  const db = ctx.db;
  const getUser = await user(parent, { id: args.id }, ctx)
  if (getUser) {
    args.userId = ObjectId(args.id);
    args.date = new Date(args.date);
    delete args.id
    const res = await db.collection('documents').insertOne({ ...args });
    return { id: res.insertedId }
  }
  throw new Error("Can't create document!");
}

export default createDocument;