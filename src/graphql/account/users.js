import { prepare } from '../utils';

const users = async (parent, args, { db }) => {
  const Users = (await db.collection('users').aggregate([
    {
      $lookup:
      {
        from: "profiles",
        localField: "_id",
        foreignField: "userId",
        as: "profiles"
      }
    },
    {
      $lookup:
      {
        from: "documents",
        localField: "_id",
        foreignField: "userId",
        as: "documents"
      }
    }
  ])
    .toArray())
    .map(user => {
      user = prepare(user);
      let profiles = user.profiles.map(prepare);
      let documents = user.documents.map(prepare);
      return { ...user, profiles: [...profiles], documents: [...documents] }
    })
  return Users;
}

export default users;