import { GraphQLServer } from 'graphql-yoga';
import { MongoClient } from 'mongodb';
import resolvers from './graphql/resolvers';

const MONGO_URL = 'mongodb://dev:dev@localhost:27017/DEV'

const start = async () => {
  
  const mongoClient = await MongoClient.connect(MONGO_URL, { useNewUrlParser: true });
  const db = mongoClient.db('DEV');

  const Server = new GraphQLServer({
    typeDefs: `${__dirname}/graphql/schema.graphql`,
    resolvers,
    context: req => ({
      ...req,
      db
    }),
  });

  // options
  const opts = {
    port: 4000,
  };

  Server.start(opts, () => {
    console.log(`Server is running on http://localhost:${opts.port}`);
  });
}

start();