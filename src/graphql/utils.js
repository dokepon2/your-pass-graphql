import jwt from 'jsonwebtoken';

export const prepare = o => {
  o.id = o._id.toString();
  return o
}

export const signToken = data => {
  return jwt.sign(data, process.env.YOURPASS_AUTH_SECRET || 'yourpass', { expiresIn: "7d" });
}

export const getAuth = ctx => {
  const authorization = ctx.request.get("Authorization");
  if (authorization) {
    const token = authorization.replace("Bearer ", "");
    const res = jwt.verify(token, process.env.YOURPASS_AUTH_SECRET || 'yourpass');
    return res;
  }
  return false;
}