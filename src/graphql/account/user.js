import { prepare, getAuth } from '../utils';

const user = async (parent, { id }, ctx) => {

  const { userId } = getAuth(ctx);
  
  if (userId) {
    const db = ctx.db;
    const Users = (await db.collection('users').aggregate([
      {
        $lookup:
        {
          from: "profiles",
          localField: "_id",
          foreignField: "userId",
          as: "profiles"
        }
      },
      {
        $lookup:
        {
          from: "documents",
          localField: "_id",
          foreignField: "userId",
          as: "documents"
        }
      },
      {
        $lookup:
        {
          from: "achievements",
          localField: "_id",
          foreignField: "userId",
          as: "achievements"
        }
      }
    ])
      .toArray())
      .map(user => {
        user = prepare(user);
        let profiles = user.profiles.map(prepare);
        let documents = user.documents.map(prepare);
        let achievements = user.achievements.map(prepare);
        return { ...user, profiles: [...profiles], documents: [...documents], achievements: [...achievements] }
      })
      .filter(user => user.id === userId);
    return Users[0];
  }
  throw new Error("Not logged in or invalid token");
}

export default user;