import * as bcrypt from 'bcryptjs';
import { signToken } from '../utils';

const authenticateUser = async (parent, { email, password }, { db }) => {
  const user = await db.collection('users').findOne({ email });
  if (user) {
    const checkValid = await bcrypt.compare(password, user.password);
    if (checkValid) {
      return {
        token: signToken({ userId: user._id, roles: user.roles }),
        id: user._id
      }
    }
  }
  throw new Error("Invalid email/password");
}

export default authenticateUser;