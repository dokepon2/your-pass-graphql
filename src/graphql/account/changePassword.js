import { ObjectId } from 'mongodb';
import * as bcrypt from 'bcryptjs';
import { signToken } from '../utils';

const changePassword = async (parent, { id, oldPassword, newPassword }, { db }) => {
  const user = prepare(await (db.collection('users').findOne({ _id: ObjectId(id) })));
  if (user) {
    const checkValid = await bcrypt.compare(oldPassword, user.password);
    if (checkValid) {
      const hash = await bcrypt.hash(newPassword, 8);
      await db.collection('users').updateOne({ _id: ObjectId(user.id) }, { $set: { password: hash } });
      return {
        id: user.id,
        token: signToken({ userId: user.id })
      }
    }
    throw new Error("Password incorrect!");
  }
  throw new Error("Can't change password!");
}

export default changePassword;